import io from 'socket.io-client';
import { onBeforeMount, reactive, ref } from 'vue';

export function useSocketIO(connectionURI: string) {
    let state = reactive({
        currentState: 'Reconnecting to server',
        UID: null,
    });

    let socketIO = ref(io(connectionURI));

    onBeforeMount(() => {
        registerListener('connect', () => {
            state.currentState = `Connected to server`;
            state.UID = socketIO.value.id;
        });
    });

    function emit(event: string, payload: any) {
        socketIO.value.emit(event, payload);
    }

    function registerListener(event: string, fn: Function) {
        socketIO.value.on(event, fn);
    }

    return { emit, registerListener, state };
}
